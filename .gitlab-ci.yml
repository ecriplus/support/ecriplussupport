stages:
  - lint
  - build
  - deploy

flake8:
  stage: lint
  image: python:3.11-alpine
  before_script:
    - pip install -q flake8
  script:
    - flake8 --ignore E501

build:
  stage: build
  image: docker:24.0.5
  services:
    - docker:24.0.5-dind
  variables:
    DOCKER_IMAGE: registry.gitlab.com/ecriplus/support/ecriplussupport
    GIT_STRATEGY: clone
    GIT_SUBMODULE_STRATEGY: recursive
    GIT_SUBMODULE_UPDATE_FLAGS: --remote
    DOCKER_BUILDKIT: 1
    TAG_BRANCH: latest
    # disable shallow clone because `git describe` needs history
    # to get previous tag
    GIT_DEPTH: ""
  needs:
    - job: flake8
      optional: false
  before_script:
    - docker info
    - apk update
    - apk upgrade
    - apk add git grep
    - git --version
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - |
      if [[ -n "$CI_COMMIT_TAG" ]]; then
        TAG_VERSION=`git describe --tags --always`
        TAG_VERSION_SHORT=`git describe --tags --always --abbrev=0`
      else
        TAG_VERSION=$CI_COMMIT_SHORT_SHA
        TAG_VERSION_SHORT=$CI_COMMIT_SHORT_SHA
      fi
    - echo "Building image $DOCKER_IMAGE:$TAG_VERSION, $DOCKER_IMAGE:$TAG_BRANCH"
    - docker build
      --pull
      --cache-from $DOCKER_IMAGE:$TAG_BRANCH
      --build-arg BUILDKIT_INLINE_CACHE=1
      --build-arg GIT_VERSION_LONG="$TAG_VERSION"
      --build-arg GIT_VERSION_SHORT="$TAG_VERSION_SHORT"
      -t $DOCKER_IMAGE:$TAG_VERSION
      -t $DOCKER_IMAGE:$TAG_BRANCH .
    - docker push $DOCKER_IMAGE:$TAG_VERSION
    - docker push $DOCKER_IMAGE:$TAG_BRANCH
    - echo "TAG_VERSION=$TAG_VERSION" >> build.env
    - echo "Built image $DOCKER_IMAGE:$TAG_VERSION"
    - echo "Built image $DOCKER_IMAGE:$TAG_BRANCH"
  artifacts:
    reports:
      dotenv: build.env
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH && $CI_COMMIT_TAG && $CI_COMMIT_TAG !~ /^v.*/
      when: never
    - if: $CI_COMMIT_MESSAGE =~ /\~NOCI/ && $CI_PIPELINE_SOURCE != "web"
      when: never
    - if: $CI_COMMIT_TAG =~ /^v.*/
      when: always
    - if: $CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH && $CI_COMMIT_SHORT_SHA
      when: always

deploy_on_helm:
  stage: deploy
  trigger:
    project: ecriplus/charts/support
  variables:
    TOOL_TAG: $TAG_VERSION
    PRODUCTION: false
  needs:
    - job: build
      optional: false
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH && $CI_COMMIT_TAG && $CI_COMMIT_TAG !~ /^v.*/
      when: never
    - if: $CI_COMMIT_MESSAGE =~ /\~NOCI/ && $CI_PIPELINE_SOURCE != "web"
      when: never
    - if: $CI_COMMIT_TAG =~ /^v.*/
      variables:
        PRODUCTION: true
      when: always
    - if: $CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH && $CI_COMMIT_SHORT_SHA
      variables:
        TOOL_TAG: $CI_COMMIT_SHORT_SHA
      when: always
