# Copyright 2013 Thatcher Peskens
# Copyright 2014-2015 Guillaume Pellerin
# Copyright 2014-2015 Thomas Fillon
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

ARG PYTHON_VERSION=3.11-slim-bookworm

FROM python:${PYTHON_VERSION} as python

MAINTAINER Guillaume Pellerin <guillaume.pellerin@iri-centrepompidou.fr>

ENV PYTHONUNBUFFERED 1

RUN mkdir -p /srv/app

WORKDIR /srv

# Install apt packages
RUN apt-get update && apt-get install --no-install-recommends -y \
  # dependencies for building Python packages
    build-essential \
    locales \
    curl \
    postgresql-client \
    python3-psycopg2 \
    libpq-dev \
    gettext \
    procps

RUN apt-get clean

RUN echo fr_FR.UTF-8 UTF-8 >> /etc/locale.gen && locale-gen
ENV LANG fr_FR.UTF-8
ENV LANGUAGE fr_FR:fr
ENV LC_ALL fr_FR.UTF-8

# upgrade pip and pin setuptools
# RUN pip3 install -U pip

# https://python-poetry.org/docs/configuration/#using-environment-variables
ENV POETRY_VERSION=1.8.3 \
    POETRY_NO_INTERACTION=1 \
    # make poetry install to this location
    POETRY_HOME="/opt/poetry" \
    PIP_CACHE_DIR="/root/.cache/pip"

ENV PATH="$POETRY_HOME/bin:$PATH"

# # Install poetry - respects $POETRY_VERSION & $POETRY_HOME
RUN curl -sSL https://install.python-poetry.org | python3 -

# Disable Poetry's virtualenv (useless in a container)
RUN poetry config virtualenvs.create false

COPY poetry.lock pyproject.toml /srv

RUN --mount=type=cache,mode=0755,target=/root/.cache/pip poetry install --no-interaction

COPY ./ecriplussupport /srv/ecriplussupport

COPY ./app /srv/app

WORKDIR /srv/app

EXPOSE 8000

CMD ["/srv/app/wsgi.sh"]
