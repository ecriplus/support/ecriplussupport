import os
import sys
from celery import Celery
from django.conf import settings
import environ

sys.path.append(os.path.dirname("."))
sys.path.append(os.path.dirname(".."))
sys.path.append("/srv")
sys.path.append("/srv/ecriplussupport")

env = environ.Env()
PRODUCTION = env.bool("PRODUCTION", False)

# set the default Django settings module for the 'celery' program.
if PRODUCTION:
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings.production")
else:
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings.local")

app = Celery("app")

# Using a string here means the worker will not have to
# pickle the object when using Windows.
app.config_from_object("django.conf:settings", namespace="CELERY")
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)
# app.conf.update(
#     CELERY_RESULT_BACKEND='djcelery.backends.database:DatabaseBackend',
# )


@app.task(bind=True)
def debug_task(self):
    print("Request: {0!r}".format(self.request))
