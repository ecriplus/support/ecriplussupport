#!/bin/bash

uid=www-data
gid=www-data

celery -A worker worker -E --uid=$uid --gid=$gid
