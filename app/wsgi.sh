#!/bin/bash

# python /srv/app/manage.py makemigrations --noinput
python /srv/app/manage.py migrate --noinput
python /srv/app/manage.py createsuperuser --noinput --username admin --email $DJANGO_SUPERUSER_EMAIL
python /srv/app/manage.py update_site --from example.com --to $DJANGO_SITE_DOMAIN
python /srv/app/manage.py collectstatic --noinput
python /srv/app/manage.py loaddata /srv/ecriplussupport/fixtures/emailtemplate.json

if [ $PRODUCTION = "True" ]; then
    gunicorn --bind :8000 --user www-data --group www-data --workers 2 --reload --reload-extra-file /srv/app/wsgi.py --log-level debug --error-logfile - --access-logfile - --capture-output wsgi
else
    python /srv/app/manage.py runserver 0.0.0.0:8000
fi
