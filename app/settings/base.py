"""
Base settings to build other settings files upon.
"""

import sys
from pathlib import Path

import environ

sys.dont_write_bytecode = True


ROOT_DIR = Path(__file__).resolve(strict=True).parent.parent.parent
# ecriplussupport/
APPS_DIR = ROOT_DIR / "ecriplussupport"
env = environ.Env()

READ_DOT_ENV_FILE = env.bool("DJANGO_READ_DOT_ENV_FILE", default=False)
if READ_DOT_ENV_FILE:
    # OS environment variables take precedence over variables from .env
    env.read_env(str(ROOT_DIR / ".env"))

# GENERAL
# ---------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#debug
DEBUG = env.bool("DJANGO_DEBUG", False)
# Local time zone. Choices are
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# though not all of them may be available with every OS.
# In Windows, this must be set to your system time zone.
TIME_ZONE = "Europe/Paris"
# https://docs.djangoproject.com/en/dev/ref/settings/#language-code
LANGUAGE_CODE = "fr"
# https://docs.djangoproject.com/en/dev/ref/settings/#site-id
SITE_ID = 1
# https://docs.djangoproject.com/en/dev/ref/settings/#use-i18n
USE_I18N = True
# https://docs.djangoproject.com/en/dev/ref/settings/#use-l10n
USE_L10N = True
# https://docs.djangoproject.com/en/dev/ref/settings/#use-tz
USE_TZ = True

LANGUAGES = [
    ("fr", "French"),
]

# https://docs.djangoproject.com/en/dev/ref/settings/#locale-paths
LOCALE_PATHS = [str(ROOT_DIR / "locale")]

# DATABASES
# -------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#databases
# DATABASES = {"default": env.db("DATABASE_URL")}
DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": env("POSTGRES_NAME", default="postgres"),
        "USER": env("POSTGRES_USER", default="postgres"),
        "PASSWORD": env("POSTGRES_PASSWORD", default="password"),
        "HOST": env("POSTGRES_HOST", default="db"),
        "PORT": env.int("POSTGRES_PORT", default=5432),
        "ATOMIC_REQUESTS": True,
    },
}

# https://docs.djangoproject.com/en/stable/ref/settings/#std:setting-DEFAULT_AUTO_FIELD
DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"

# URLS
# -------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#root-urlconf
ROOT_URLCONF = "app.urls"
# https://docs.djangoproject.com/en/dev/ref/settings/#wsgi-application
WSGI_APPLICATION = "wsgi.application"

# APPS
# ------------------------------------------------------------------------------
INSTALLED_APPS = [
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.sites",
    "django.contrib.messages",
    "collectfasta",
    "django.contrib.staticfiles",
    "django.contrib.humanize",  # Handy template tags
    "django.contrib.admin",
    "django.forms",
    "crispy_forms",
    "crispy_bootstrap5",
    "bootstrap4",
    "bootstrap4form",
    "account",
    "martor",
    "rest_framework",  # required for the API
    "pinax.invitations",
    "pinax.teams",
    "ecriplussupport",
    "helpdesk",  # django-helpdesk app
    "djcelery_email",
    "django_celery_beat",
    "django_celery_results",
    # "corsheaders",
]

# MIGRATIONS
# ---------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#migration-modules
# MIGRATION_MODULES = {"ecriplussupport": "ecriplussupport.migrations"}

# AUTHENTICATION
# ---------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#authentication-backends
AUTHENTICATION_BACKENDS = [
    "django.contrib.auth.backends.ModelBackend",
]
# https://docs.djangoproject.com/en/dev/ref/settings/#auth-user-model
# AUTH_USER_MODEL = "users.User"
# https://docs.djangoproject.com/en/dev/ref/settings/#login-redirect-url
LOGIN_REDIRECT_URL = "/dashboard/"
# https://docs.djangoproject.com/en/dev/ref/settings/#login-url
LOGIN_URL = "/accounts/login/"

DJANGO_ADMIN_URL = "/admin/"

# PASSWORDS
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#password-hashers
PASSWORD_HASHERS = [
    # https://docs.djangoproject.com/en/dev/topics/auth/passwords/#using-argon2-with-django
    "django.contrib.auth.hashers.Argon2PasswordHasher",
    "django.contrib.auth.hashers.PBKDF2PasswordHasher",
    "django.contrib.auth.hashers.PBKDF2SHA1PasswordHasher",
    "django.contrib.auth.hashers.BCryptSHA256PasswordHasher",
]
# https://docs.djangoproject.com/en/dev/ref/settings/#auth-password-validators
AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator"
    },
    {"NAME": "django.contrib.auth.password_validation.MinimumLengthValidator"},
    {"NAME": "django.contrib.auth.password_validation.CommonPasswordValidator"},
    {"NAME": "django.contrib.auth.password_validation.NumericPasswordValidator"},
]

# MIDDLEWARE
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#middleware
MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    # "whitenoise.middleware.WhiteNoiseMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.locale.LocaleMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "account.middleware.LocaleMiddleware",
    "account.middleware.TimezoneMiddleware",
    "account.middleware.ExpiredPasswordMiddleware",
    "django.middleware.common.BrokenLinkEmailsMiddleware",
    # "django.middleware.clickjacking.XFrameOptionsMiddleware",
    # "corsheaders.middleware.CorsMiddleware",
    "django.middleware.common.CommonMiddleware",
]

# STATIC
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#static-root
STATIC_ROOT = "/srv/static"

# https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#std:setting-STATICFILES_DIRS
STATICFILES_DIRS = [str(APPS_DIR / "static")]
# https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#staticfiles-finders
STATICFILES_FINDERS = [
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
]
# https://docs.djangoproject.com/en/dev/ref/settings/#static-url
STATIC_URL = "/static/"

# MEDIA
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#media-root
MEDIA_ROOT = "/srv/media"
# https://docs.djangoproject.com/en/dev/ref/settings/#media-url
MEDIA_URL = "/media/"

# TEMPLATES
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#templates
TEMPLATES = [
    {
        # https://docs.djangoproject.com/en/dev/ref/settings/#std:setting-TEMPLATES-BACKEND
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        # https://docs.djangoproject.com/en/dev/ref/settings/#dirs
        "DIRS": [str(APPS_DIR / "templates")],
        # https://docs.djangoproject.com/en/dev/ref/settings/#app-dirs
        "APP_DIRS": True,
        "OPTIONS": {
            # https://docs.djangoproject.com/en/dev/ref/settings/#template-context-processors
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.template.context_processors.i18n",
                "django.template.context_processors.media",
                "django.template.context_processors.static",
                "django.template.context_processors.tz",
                "django.contrib.messages.context_processors.messages",
                "account.context_processors.account",
            ],
        },
    }
]

# https://docs.djangoproject.com/en/dev/ref/settings/#form-renderer
FORM_RENDERER = "django.forms.renderers.TemplatesSetting"

# http://django-crispy-forms.readthedocs.io/en/latest/install.html#template-packs
CRISPY_TEMPLATE_PACK = "bootstrap5"
CRISPY_ALLOWED_TEMPLATE_PACKS = "bootstrap5"

# FIXTURES
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#fixture-dirs
# FIXTURE_DIRS = (str(APPS_DIR / "fixtures"),)

# SECURITY
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#session-cookie-httponly
SESSION_COOKIE_HTTPONLY = True
# https://docs.djangoproject.com/en/dev/ref/settings/#csrf-cookie-httponly
CSRF_COOKIE_HTTPONLY = True
# https://docs.djangoproject.com/en/dev/ref/settings/#secure-browser-xss-filter
SECURE_BROWSER_XSS_FILTER = True
# https://docs.djangoproject.com/en/dev/ref/settings/#x-frame-options
X_FRAME_OPTIONS = "DENY"

# EMAIL
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#email-backend
EMAIL_BACKEND = env(
    "DJANGO_EMAIL_BACKEND",
    default="djcelery_email.backends.CeleryEmailBackend",
)
# https://docs.djangoproject.com/en/dev/ref/settings/#email-timeout
EMAIL_TIMEOUT = 5

# ADMIN
# ------------------------------------------------------------------------------
# Django Admin URL.
ADMIN_URL = "/admin/"
# https://docs.djangoproject.com/en/dev/ref/settings/#admins
ADMINS = [("Guillaume Pellerin", "guillaume.pellerin@iri-centrepompidou.fr")]
# https://docs.djangoproject.com/en/dev/ref/settings/#managers
MANAGERS = ADMINS

# LOGGING
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#logging
# See https://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "verbose": {
            "format": "%(levelname)s %(asctime)s %(module)s "
            "%(process)d %(thread)d %(message)s"
        }
    },
    "handlers": {
        "console": {
            "level": "DEBUG",
            "class": "logging.StreamHandler",
            "formatter": "verbose",
        }
    },
    "root": {"level": "INFO", "handlers": ["console"]},
}


# CACHE
# ------------------------------------------------

CACHES = {
    "default": {
        "BACKEND": "django.core.cache.backends.redis.RedisCache",
        "LOCATION": env.str("REDIS_URL"),
    }
}


#
# ------------------------------------------------------------------------------
ACCOUNT_OPEN_SIGNUP = env.bool("DJANGO_ACCOUNT_ALLOW_REGISTRATION", True)
ACCOUNT_SIGNUP_URL = "account_signup"
ACCOUNT_LOGIN_URL = "account_login"
ACCOUNT_LOGOUT_URL = "account_logout"
ACCOUNT_SIGNUP_REDIRECT_URL = "/"
ACCOUNT_LOGIN_REDIRECT_URL = "/"
ACCOUNT_LOGOUT_REDIRECT_URL = "/"
ACCOUNT_PASSWORD_CHANGE_REDIRECT_URL = "account_password"
ACCOUNT_PASSWORD_RESET_REDIRECT_URL = "account_login"
ACCOUNT_PASSWORD_RESET_TOKEN_URL = "account_password_reset_token"
ACCOUNT_PASSWORD_EXPIRY = 0
ACCOUNT_PASSWORD_USE_HISTORY = False
ACCOUNT_PASSWORD_STRIP = True
ACCOUNT_REMEMBER_ME_EXPIRY = 60 * 60 * 24 * 365 * 10
ACCOUNT_USER_DISPLAY = lambda user: user.username  # noqa
ACCOUNT_CREATE_ON_SAVE = True
ACCOUNT_EMAIL_UNIQUE = True
ACCOUNT_EMAIL_CONFIRMATION_REQUIRED = True
ACCOUNT_EMAIL_CONFIRMATION_EMAIL = True
ACCOUNT_EMAIL_CONFIRMATION_EXPIRE_DAYS = 3
ACCOUNT_EMAIL_CONFIRMATION_AUTO_LOGIN = False
ACCOUNT_EMAIL_CONFIRMATION_ANONYMOUS_REDIRECT_URL = "account_login"
ACCOUNT_EMAIL_CONFIRMATION_AUTHENTICATED_REDIRECT_URL = None
ACCOUNT_EMAIL_CONFIRMATION_URL = "account_confirm_email"
ACCOUNT_SETTINGS_REDIRECT_URL = "account_settings"
ACCOUNT_NOTIFY_ON_PASSWORD_CHANGE = True
ACCOUNT_DELETION_EXPUNGE_HOURS = 48
ACCOUNT_DEFAULT_HTTP_PROTOCOL = "https"
ACCOUNT_HOOKSET = "account.hooks.AccountDefaultHookSet"
# ACCOUNT_TIMEZONES = list(zip(pytz.all_timezones, pytz.all_timezones))
# https://django-user-accounts.readthedocs.io/en/latest/settings.html
# ACCOUNT_LANGUAGES = []
# https://django-user-accounts.readthedocs.io/en/latest/settings.html


# HELPDESK
# -----------------------------------------------------

HELPDESK_DEFAULT_SETTINGS = {
    "use_email_as_submitter": env.bool("HELPDESK_USE_EMAIL_AS_SUBMITTER", True),
    "email_on_ticket_assign": env.bool("HELPDESK_EMAIL_ON_TICKET_ASSIGN", True),
    "email_on_ticket_change": env.bool("HELPDESK_EMAIL_ON_TICKET_CHANGE", True),
    "login_view_ticketlist": env.bool("HELPDESK_LOGIN_VIEW_TICKETLIST", True),
    "email_on_ticket_apichange": env.bool("HELPDESK_EMAIL_ON_TICKET_APICHANGE", True),
    "preset_replies": env.bool("HELPDESK_PRESET_REPLIES", True),
    "tickets_per_page": env.bool("HELPDESK_TICKETS_PER_PAGE", "25"),
}

# Should the public web portal be enabled?
HELPDESK_PUBLIC_ENABLED = env.bool("HELPDESK_PUBLIC_ENABLED", True)
# HELPDESK_VIEW_A_TICKET_PUBLIC = env.bool("HELPDESK_VIEW_A_TICKET_PUBLIC", True)
HELPDESK_VIEW_A_TICKET_PUBLIC = False
HELPDESK_SUBMIT_A_TICKET_PUBLIC = env.bool("HELPDESK_SUBMIT_A_TICKET_PUBLIC", True)

# Should the Knowledgebase be enabled?
HELPDESK_KB_ENABLED = env.bool("HELPDESK_KB_ENABLED", False)

HELPDESK_TICKETS_TIMELINE_ENABLED = env.bool("HELPDESK_TICKETS_TIMELINE_ENABLED", True)

# Allow users to change their passwords
HELPDESK_SHOW_CHANGE_PASSWORD = env.bool("HELPDESK_SHOW_CHANGE_PASSWORD", True)

# Permet de cacher le champs "Résolution souhaité le :" la valeur "None" permet de ne pas attribuer de valeur
HELPDESK_PUBLIC_TICKET_DUE_DATE = False

# Mise en place de la File/Queue par défaut et suppression du field "File" dans le formulaire
HELPDESK_PUBLIC_TICKET_QUEUE = env.str(
    "HELPDESK_PUBLIC_TICKET_QUEUE", default="ecriplussupport"
)

HELPDESK_PUBLIC_TICKET_PRIORITY = 3

HELPDESK_USE_CDN = False

HELPDESK_TEAMS_MODE_ENABLED = False

HELPDESK_REDIRECT_TO_LOGIN_BY_DEFAULT = False

# MARTOR
# --------------------------------------------------------------------

# Choices are: "semantic", "bootstrap"
MARTOR_THEME = "bootstrap"

# Global martor settings
# Input: string boolean, `true/false`
MARTOR_ENABLE_CONFIGS = {
    "emoji": "true",  # to enable/disable emoji icons.
    "imgur": "false",  # to enable/disable imgur/custom uploader.
    "mention": "false",  # to enable/disable mention
    "jquery": "true",  # to include/revoke jquery (require for admin default django)
    "living": "true",  # to enable/disable live updates in preview
    "spellcheck": "false",  # to enable/disable spellcheck in form textareas
    "hljs": "true",  # to enable/disable hljs highlighting in preview
}

# To show the toolbar buttons
MARTOR_TOOLBAR_BUTTONS = [
    "bold",
    "italic",
    "horizontal",
    "heading",
    "pre-code",
    "blockquote",
    "unordered-list",
    "ordered-list",
    "link",
    "image-link",
    "image-upload",
    "emoji",
    "direct-mention",
    "toggle-maximize",
    "help",
]

# To setup the martor editor with title label or not (default is False)
MARTOR_ENABLE_LABEL = False

# Imgur API Keys
# MARTOR_IMGUR_CLIENT_ID = 'your-client-id'
# MARTOR_IMGUR_API_KEY   = 'your-api-key'

# Markdownify
MARTOR_MARKDOWNIFY_FUNCTION = "martor.utils.markdownify"  # default
MARTOR_MARKDOWNIFY_URL = "/martor/markdownify/"  # default

# Markdown extensions (default)
MARTOR_MARKDOWN_EXTENSIONS = [
    "markdown.extensions.extra",
    "markdown.extensions.nl2br",
    "markdown.extensions.smarty",
    "markdown.extensions.fenced_code",
    # Custom markdown extensions.
    # 'martor.extensions.urlize',
    # 'martor.extensions.del_ins',      # ~~strikethrough~~ and ++underscores++
    # 'martor.extensions.mention',      # to parse markdown mention
    # 'martor.extensions.emoji',        # to parse markdown emoji
    # 'martor.extensions.mdx_video',    # to parse embed/iframe video
    # 'martor.extensions.escape_html',  # to handle the XSS vulnerabilities
]

# Markdown Extensions Configs
MARTOR_MARKDOWN_EXTENSION_CONFIGS = {}

# Markdown urls
MARTOR_UPLOAD_URL = "/martor/uploader/"  # default
MARTOR_SEARCH_USERS_URL = "/martor/search-user/"  # default

# Markdown Extensions
# MARTOR_MARKDOWN_BASE_EMOJI_URL = 'https://www.webfx.com/tools/emoji-cheat-sheet/graphics/emojis/'     # from webfx
MARTOR_MARKDOWN_BASE_EMOJI_URL = (
    "https://github.githubassets.com/images/icons/emoji/"  # default from github
)
MARTOR_MARKDOWN_BASE_MENTION_URL = (
    "https://python.web.id/author/"  # please change this to your domain
)

# If you need to use your own themed "bootstrap" or "semantic ui" dependency
# replace the values with the file in your static files dir
# MARTOR_ALTERNATIVE_JS_FILE_THEME = "semantic-themed/semantic.min.js"   # default None
# MARTOR_ALTERNATIVE_CSS_FILE_THEME = "semantic-themed/semantic.min.css" # default None
# MARTOR_ALTERNATIVE_JQUERY_JS_FILE = "jquery/dist/jquery.min.js"        # default None

# URL schemes that are allowed within links
ALLOWED_URL_SCHEMES = [
    "file",
    "ftp",
    "ftps",
    "http",
    "https",
    "irc",
    "mailto",
    "sftp",
    "ssh",
    "tel",
    "telnet",
    "tftp",
    "vnc",
    "xmpp",
]

CSRF_COOKIE_HTTPONLY = False

# S3
# -------------------------------------------------------

if env.bool("AWS_S3_USE", False):
    DEFAULT_FILE_STORAGE = "ecriplussupport.storages.PublicMediaStorage"

    STORAGES = {
        "default": {
            "BACKEND": "ecriplussupport.storages.PublicMediaStorage",
        },
        "staticfiles": {
            "BACKEND": "ecriplussupport.storages.StaticStorage",
        },
    }

    STATIC_LOCATION = "static"
    PUBLIC_MEDIA_LOCATION = "media"

    AWS_ACCESS_KEY_ID = env.str("AWS_ACCESS_KEY_ID", "admin")
    AWS_SECRET_ACCESS_KEY = env.str("AWS_SECRET_ACCESS_KEY", "admin")
    AWS_STORAGE_BUCKET_NAME = env.str("AWS_STORAGE_BUCKET_NAME", "ecriplussupport")
    AWS_S3_ENDPOINT_URL = env.str("AWS_S3_ENDPOINT_URL", "http://minio:9000/")
    AWS_S3_REGION_NAME = env.str("AWS_S3_REGION_NAME", "")

    AWS_DEFAULT_ACL = None
    AWS_QUERYSTRING_AUTH = env.bool("AWS_QUERYSTRING_AUTH", False)
    AWS_S3_OBJECT_PARAMETERS = {"CacheControl": "max-age=86400"}
    AWS_S3_FILE_OVERWRITE = env.bool("AWS_S3_FILE_OVERWRITE", False)
    AWS_S3_ENCRYPTION = env.bool("AWS_S3_ENCRYPTION", False)
    AWS_S3_USE_SSL = env.bool("AWS_S3_USE_SSL", False)
    AWS_S3_URL_PROTOCOL = env.str("AWS_S3_URL_PROTOCOL", "")

    if not env.bool("PRODUCTION", False):
        AWS_S3_URL_PROTOCOL = "http:"
        AWS_S3_CUSTOM_DOMAIN = f"localhost:9000/{AWS_STORAGE_BUCKET_NAME}"
        MEDIA_URL = (
            f"{AWS_S3_URL_PROTOCOL}{AWS_S3_CUSTOM_DOMAIN}/{PUBLIC_MEDIA_LOCATION}/"
        )
        STATIC_URL = f"{AWS_S3_URL_PROTOCOL}//{AWS_S3_CUSTOM_DOMAIN}/{STATIC_LOCATION}/"
    else:
        # works with OVH Cloud buckets
        CUSTOM_DOMAIN = AWS_S3_ENDPOINT_URL.split("/")[2]
        AWS_S3_CUSTOM_DOMAIN = f"{AWS_STORAGE_BUCKET_NAME}.{CUSTOM_DOMAIN}"
        MEDIA_URL = f"{AWS_S3_CUSTOM_DOMAIN}/{PUBLIC_MEDIA_LOCATION}/"
        STATIC_URL = f"{AWS_S3_CUSTOM_DOMAIN}/{STATIC_LOCATION}/"

else:
    STORAGES = {
        "default": {
            "BACKEND": "django.core.files.storage.FileSystemStorage",
        },
        "staticfiles": {
            "BACKEND": "django.contrib.staticfiles.storage.StaticFilesStorage",
        },
    }

    MEDIA_URL = "/media/"
    STATIC_URL = "/static/"

# COLLECFASTA
# -------------------------------------------------------
COLLECTFASTA_ENABLED = True
COLLECTFASTA_STRATEGY = "collectfasta.strategies.boto3.Boto3Strategy"
COLLECTFASTA_THREADS = 8


# CELERY
# ----------------------------------------------------------

CELERY_BACKEND_URL = env.str("REDIS_URL") + "/0"
CELERY_BROKER_TRANSPORT = "redis"
CELERY_BROKER_URL = env.str("REDIS_URL") + "/0"
CELERY_TASK_SERIALIZER = "json"
CELERY_ACCEPT_CONTENT = ["application/json"]
CELERY_TASK_ALWAYS_EAGER = True
CELERY_RESULT_BACKEND = "django-db"
CELERY_TIMEZONE = TIME_ZONE

CELERY_EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"
CELERY_EMAIL_TASK_CONFIG = {
    "queue": "django_email",
    "delivery_mode": 1,  # non persistent
    "rate_limit": "50/m",  # 50 chunks per minute
}

CELERY_BEAT_SCHEDULER = "django_celery_beat.schedulers:DatabaseScheduler"

# ECRIPLUS
# ----------------------------------------------------------

ECRIPLUSTESTS_CHALLENGES_BASE_URL = env.str("ECRIPLUSTESTS_CHALLENGES_BASE_URL", "")
