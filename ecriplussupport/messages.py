# coding: utf-8


def _():
    return lambda s: s


django_standard_messages_to_override = [
    _("Please be as descriptive as possible and include all details"),
    _(
        'If you want to update state of checklist tasks, please do a Follow-Up response and click on "Update checklists"',
    ),
]
