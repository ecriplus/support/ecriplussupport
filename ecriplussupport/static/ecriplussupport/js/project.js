/* Project specific Javascript goes here. */

/* Fix a bootstrap4form bug which invert a checkbox and its related label */
/* here applied to #id_custom_cgu */

$(document).ready(function() {
    if($('#id_custom_cgu').length) {
        let button = $("#id_custom_cgu");
        let div = button.parent().parent();
        button.detach();
        button.appendTo(div);
    }
});

/* Hide code_question input */

$(document).ready(function() {
    let params = new URLSearchParams(window.location.search);
    if (params.has('code_question')) {
        let param = params.get('code_question');
        let field = $("#id_custom_code_question");
        field.val(param);
        field.hide();
        let label = $('label[for="id_custom_code_question"]');
        label.hide();
    }
    if (params.has('email')) {
        let param = params.get('email');
        console.log(param);
        let field = $("#id_submitter_email");
        field.val(param);
        let div = field.parent().parent();
        div.hide();
    }
});
