from django.core.management.base import BaseCommand
from django.contrib.sites.models import Site


class Command(BaseCommand):
    help = "Change Site domain to another"

    def add_arguments(self, parser):
        parser.add_argument(
            "--from",
            type=str,
            required=True,
            help="from domain",
        )
        parser.add_argument(
            "--to",
            type=str,
            required=True,
            help="to domain",
        )

    def handle(self, *args, **options):
        from_domain = options["from"]
        to_domain = options["to"]

        if "," in to_domain:
            to_domain = to_domain.split(",")[0]

        sites = Site.objects.filter(domain=from_domain)
        if sites:
            site = sites[0]
            site.domain = to_domain
            site.name = to_domain
            site.save()
