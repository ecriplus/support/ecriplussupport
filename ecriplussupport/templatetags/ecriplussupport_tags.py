from django import template
from django.conf import settings

from ecriplussupport import __version__

register = template.Library()


class AppVersionNode(template.Node):
    def render(self, context):
        return __version__


@register.tag
def app_version(parser, token):
    "Get app version number"
    return AppVersionNode()


@register.tag
def value_from_settings(parser, token):
    try:
        # split_contents() knows not to split quoted strings.
        tag_name, var = token.split_contents()
    except ValueError:
        raise template.TemplateSyntaxError(
            "%r tag requires a single argument" % token.contents.split()[0]
        )
    return ValueFromSettings(var)


class ValueFromSettings(template.Node):
    def __init__(self, var):
        self.arg = template.Variable(var)

    def render(self, context):
        return settings.__getattr__(str(self.arg))
