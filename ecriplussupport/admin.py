from django.contrib import admin
from django.db import models
from helpdesk.admin import KBItemAdmin
from helpdesk.models import KBItem
from martor.widgets import AdminMartorWidget


class KBItemAdminOverride(KBItemAdmin):
    formfield_overrides = {
        models.TextField: {"widget": AdminMartorWidget},
    }


admin.site.unregister(KBItem)
admin.site.register(KBItem, KBItemAdminOverride)
