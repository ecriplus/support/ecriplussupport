# Support Ecri+

Ticket tracker application for the Ecri+ project based on django-helpdesk.


## Architecture

This application uses the [Django framework](https://www.djangoproject.com/) and the [django-helpdesk](https://github.com/django-helpdesk/django-helpdesk/) module.

It is configured so that every static and media files are managed through a S3 external service using the django-storages module. This ensure that the deploy application need no any volume or filesystem to run. This behavior can be disabled using the `AWS_S3_USE` environment variable.

Python dependencies are managed with Poetry, see pyproject.toml for more details.

The app packaged with Docker. A basic Docker composition is provided using Minio as the S3 service and Mailhog as a local testing web server.

### Docker

See detailed [cookiecutter-django Docker documentation](http://cookiecutter-django.readthedocs.io/en/latest/deployment-with-docker.html).

### Email Server

In development, it is often nice to be able to see emails that are being sent from your application. For that reason local SMTP server [MailHog](https://github.com/mailhog/MailHog) with a web interface is available as docker container.

Container mailhog will start automatically when you will run all docker containers.
Please check [cookiecutter-django Docker documentation](http://cookiecutter-django.readthedocs.io/en/latest/deployment-with-docker.html) for more details how to start all containers.

With MailHog running, to view messages that are sent by your application, open your browser and go to `http://127.0.0.1:8025`


## Environments

### Docker environments

All customizable environment variables are listed in `.env.example`. This file can be copied to `.env` in order to start a local instance using docker compose. All variables can also be overriden by an orchestrator.

To start the app locally for development, you need to set `PRODUCTION=False` and `DJANGO_DEBUG=True` in the environment.

To deploy it in production, you need to set `PRODUCTION=True` and `DJANGO_DEBUG=False` in the environment.

To start it with Docker as a compostion, copy `.env.example` to `.env` and adapt the values. Then you can assign the `docker-compose-dev.yml` or `docker-compose.yml` to the `COMPOSE_FILE` to switch between the development mode and the production mode.

### Django settings

Settings are mainly based on the [cookiecutter setup](http://cookiecutter-django.readthedocs.io/en/latest/settings.html).

`base.py` contains basic settings of which some values are taken from the environment.

`dev.py` contains special settings for development based on the django runserver.

`production.py` contains special settings for production.

`local.yml` contains special settings for local developments (hence loaded at the very end of booting process).


### Proxying

In `PRODUCTION` mode, just use a reverse proxy (like Nginx with a `proxy_pass` rule) to the port 8888 of the app container.


## Other variables

see env/staging.env.example


### Custom Bootstrap Compilation

The generated CSS is set up with automatic Bootstrap recompilation with variables of your choice.
Bootstrap v5 is installed using npm and customised by tweaking your variables in `ecriplussupport/static/sass/custom_bootstrap_vars`.

You can find a list of available variables [in the bootstrap source](https://github.com/twbs/bootstrap/blob/main/scss/_variables.scss), or get explanations on them in the [Bootstrap docs](https://getbootstrap.com/docs/5.1/customize/sass/).

Bootstrap's javascript as well as its dependencies is concatenated into a single file: `ecriplussupport/static/js/vendors.js`.

### Embedding the ticket submission form

The ticket submission form can be embedded into an iframe or a modal. The URL used should provide an argument to feed the `code_question` and `email` custom field like this:

https://DOMAIN/tickets/submit_iframe/?code_question=CODE&email=foo@bar.org

where DOMAIN is the domain of the ecriplussupport instance and CODE the code of the question coming from the external application.


## License

Ecriplussuport is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Ecriplusupport is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

Please read the LICENSE.txt file for more details.
